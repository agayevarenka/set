import java.util.*;

public class Main {
    public static void main(String[] args) {
//        HashSet
        Set<String> namesH = new HashSet<>();
        namesH.add("Ali");
        namesH.add("Ferid");
        namesH.add("Amil");
        namesH.add("Rena");
        System.out.println(namesH);
        if (namesH.contains("Amil")) {
            System.out.println("present");
        } else {
            System.out.println("not present");
        }
        namesH.forEach(System.out::println);
        namesH.remove("Rena");
        System.out.println(namesH.isEmpty());
        System.out.println(namesH);
        namesH.clear();
        System.out.println(namesH);
        Iterator<String> iterator1 = namesH.iterator();
        System.out.println(iterator1.next());
        System.out.println(iterator1.next());
        System.out.println(iterator1.next());
        System.out.println(iterator1.next());

        //LinkedHashSet
        Set<String> namesL = new LinkedHashSet<>();
        namesL.add("dog");
        namesL.add("cat");
        namesL.add("bird");
        namesL.add("alligator");
        System.out.println(namesL);
        Iterator<String> iterator2 = namesL.iterator();
        while (iterator2.hasNext()) {
            String element = iterator2.next();
            System.out.println(element);
        }

//        TreeSet
        Set<Integer> namesIntTree = new TreeSet<>();
        namesIntTree.add(40);
        namesIntTree.add(60);
        namesIntTree.add(20);
        namesIntTree.add(30);
        namesIntTree.add(50);
        namesIntTree.add(30);
        namesIntTree.add(10);
        System.out.println(namesIntTree);

    }
}
